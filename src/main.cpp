/*
MIT License

Copyright (c) 2018 dpoole1337

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>

// #include <iostream> // for amateurish debugging

#include "../include/collision.h" // collision functions

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "shooter demo"); //make a window
    window.setVerticalSyncEnabled(true); //kinda slow

    /* WARNING: variables */

    //font
    sf::Font font;
    if (!font.loadFromFile("C:/Windows/Fonts/Arial.ttf")) //bad practice? sure
    {
        // error...
        return -1;
    }

    //game over text
    sf::Text gameover;
    gameover.setString("Game Over!");
    gameover.setCharacterSize(60);
    gameover.setFillColor(sf::Color::Red);
    gameover.setFont(font);
    gameover.setPosition(250, 400);

    //HUD stuff
    sf::Text fps;
    fps.setString("FPS: 0");
    fps.setCharacterSize(15);
    fps.setFont(font);

    sf::Text hudspeed;
    hudspeed.setString("Speed: 3");
    hudspeed.setCharacterSize(15);
    hudspeed.setFont(font);
    hudspeed.setPosition(0, 16);

    while(!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) //effectively useless because this loop has a pause between tries, but good practices eh?
    {
        //begin game
        //player
        sf::CircleShape player(10); //create player
        player.setFillColor(sf::Color::Yellow); // set to yellow
        player.setPosition(200, 300); // put in left center

        //enemy
        sf::CircleShape badguy(10); //create badguy
        badguy.setFillColor(sf::Color::Red); // set to red
        badguy.setPosition(600, 300); // put in right center

        //obstacles
        sf::CircleShape obstacle1(50); //create obstacle
        obstacle1.setFillColor(sf::Color::White); // set to white
        obstacle1.setPosition(400, 250); // put in right center

        // general
        int speed = 3;
        bool alive = true;
        sf::Clock timealive;
        sf::Clock gameclock;
        int frame;
        int speedclock = 0;
        hudspeed.setString("Speed: 3");

        /* You're safe now */

        while (window.isOpen() && alive) //game loop
        {
            window.clear(); //clear the screen
            sf::Event event; //event
            frame++;

            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)//if they close the window
                {
                    window.close(); // get out of dodge
                }
            }

            //player movement

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) // W pressed
            {
                if(player.getPosition().y - speed - 2 > 0)
                {
                    player.move(0, -speed - 2); // move up
                    if(KCollision::circleCollision(player.getPosition(), obstacle1.getPosition(), player.getRadius(), obstacle1.getRadius()) == true)
                    {
                        player.move(0, speed + 2); //move back
                    }
                }
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) // A pressed
            {
                if(player.getPosition().x - speed - 2 > 0)
                {
                    player.move(-speed - 2, 0); // move left
                    if(KCollision::circleCollision(player.getPosition(), obstacle1.getPosition(), player.getRadius(), obstacle1.getRadius()) == true)
                    {
                        player.move(speed + 2, 0); //move back
                    }
                }
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) // S pressed
            {
                if(player.getPosition().y + speed + 2 < 580)
                {
                    player.move(0, speed + 2); // move down
                    if(KCollision::circleCollision(player.getPosition(), obstacle1.getPosition(), player.getRadius(), obstacle1.getRadius()) == true)
                    {
                        player.move(0, -speed - 2); //move back
                    }
                }
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) // D pressed
            {
                if(player.getPosition().x + speed + 2 < 780)
                {
                    player.move(speed + 2, 0); // move right
                    if(KCollision::circleCollision(player.getPosition(), obstacle1.getPosition(), player.getRadius(), obstacle1.getRadius()) == true)
                    {
                        player.move(-speed - 2, 0); //move back
                    }
                }
            }

            //AI stuff
            if(player.getPosition().x > badguy.getPosition().x) // right of enemy
            {
                badguy.move(speed, 0); //move right
            }
            if(player.getPosition().x < badguy.getPosition().x) // left of enemy
            {
                badguy.move(-speed, 0); //move left
            }
            if(player.getPosition().y > badguy.getPosition().y) // below enemy
            {
                badguy.move(0, speed); // move down
            }
            if(player.getPosition().y < badguy.getPosition().y) // above enemy
            {
                badguy.move(0, -speed); // move up
            }

            //std::cout << player.getPosition().x << ", " << player.getPosition().y << std::endl;
            //std::cout << badguy.getPosition().x << ", " << badguy.getPosition().y << std::endl << std::endl;

            window.draw(player);
            window.draw(badguy);
            window.draw(obstacle1);

            //change fps
            window.draw(fps);

            //change speed/difficulty
            window.draw(hudspeed);

            if(KCollision::circleCollision(player.getPosition(), badguy.getPosition(), player.getRadius(), badguy.getRadius()) == true)
            {
                window.draw(gameover); // GAME OVER, YEAH
                alive = false; // you're not alive lmao
            }
            if(speedclock >= 10) //increase difficulty
            {
                speed++;
                hudspeed.setString("Speed " + std::to_string(speed));
                speedclock = 0;
            }

            if(gameclock.getElapsedTime().asSeconds() >= 1) //FPS
            {
                fps.setString("FPS " + std::to_string((frame)/(gameclock.getElapsedTime().asSeconds())));
                speedclock++;
                gameclock.restart();
                frame = 0;
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                break;
            }

            window.display(); //put everything on screen
        }
        gameclock.restart();
        while(gameclock.getElapsedTime().asSeconds() <= 2)
        {
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                {
                    break;
                }
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            break;
        }
    }
    return EXIT_SUCCESS;
}
